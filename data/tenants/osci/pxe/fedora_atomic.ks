text

url --url="https://mirror.i3d.net/pub/fedora/linux/releases/27/Server/x86_64/os/"

keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
timezone America/New_York --isUtc --ntpservers=speedy.osci.io,guido.osci.io

# Network information
network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate
network  --bootproto=dhcp --device=eth1 --ipv6=auto --activate
network  --bootproto=dhcp --device=eth2 --ipv6=auto
network  --bootproto=dhcp --device=eth3 --ipv6=auto
network  --hostname={{ hostname }}

rootpw --iscrypted {{ root_pw |password_hash('sha512', hash) }}
sshkey --username=root "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFF7fG1/v1CJmIGjRMlPKOwPt6gUOxmoWMLGh79PakIngrj/We3fZRDj8OOgtW07SmYqfeoIpg168UMTbJBwrEw= /home/duck/.ssh/id_ecdsa"
authconfig --enableshadow --passalgo=sha512 --enablefingerprint

services --enabled="chronyd,sshd"

ignoredisk --only-use=sda
zerombr
bootloader --location=mbr --boot-drive=sda
clearpart --all --initlabel --drives=sda
part /boot --fstype=ext4 --size=500 --asprimary --ondisk=sda
part pv.01 --grow --size=5000 --ondisk=sda
volgroup vg_root_{{ name }} pv.01
#logvol swap  --fstype="swap" --size=1024 --name=swap --vgname=vg_root_{{ name }}
logvol / --vgname=vg_root_{{ name }} --fstype={{ filesystem }} --grow --size=5000 --maxsize=8000 --name=root

ostreesetup --nogpg --osname="fedora-atomic" --remote="fedora-atomic-27" --url="https://pxe.osci.io/atomic/27" --ref="fedora/27/x86_64/atomic-host"

reboot

#%packages
#openssh-server
#python
#%end

#%post --log=/root/ansible_post.log
#config="/etc/ssh/sshd_config"
#if grep -q '^PermitRootLogin ' $config; then
#    sed 's/^PermitRootLogin .*/PermitRootLogin without-password/' -i $config
#else
#    echo "PermitRootLogin without-password" >> $config
#fi;
#%end

