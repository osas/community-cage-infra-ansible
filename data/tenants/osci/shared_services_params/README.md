# OSCI Zone

These parameters are maintained by OSPO/OSCI to describe the Community
Cage hosting and OSCI shared services used by certain hosts in this
project.

Please contact us before making modifications.

